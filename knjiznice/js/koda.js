/* global $ */ 
/* global btoa */
/* global Plotly */
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var ehrId = "";

var visTez = [[150, 60], [190, 70], [175, 75]];

var EHR_ID = ["", "522dd0bd-901b-4b10-a0a8-b0844aa8caf4", "84c8f91d-ce13-4324-8d75-bc6475a35996", "ec2f6ff2-0a40-4ca1-824b-ffe29556a892"];

var graf = [];
var layout = {
  autosize: false,
  width: 550,
  height: 550,
  yaxis: {range: [0, 35]},
  margin: {
    l: 25,
    r: 25,
    b: 50,
    t: 50,
    pad: 4
  },
  plot_bgcolor: '#c7c7c7'
};

var BMI = [26.7, 24.5, 19.4];
var stevec = 3;

function plotaj(){
  if ($("#Graf").length > 0) {
    Plotly.newPlot('Graf', graf, layout);
  }
}

function preveriGraf(ime){
  for(var i = 0; i < graf[0].x.length; i++){
    if(graf[0].x[i] == ime) {
      return i;
    }
  }
  return -1;
}

function Master(){
  var str = "</br> <h4>Trenutni uporabniki aplikacije:</h4>";
  for(var i = 0; i < stevec; i++){
    str += "&nbsp&nbsp&nbsp- " + graf[0].x[i] + " (višina: " + visTez[i][0] + ", teza: " + visTez[i][1] + ") ima BMI: " + graf[0].y[i] + "</br>";
  }
  $("#master").html(str);
}


$(document).ready(function(){
  /*var bla ="";
  $.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
          ehrId = data.ehrId;
          //EHR_ID[stPacienta] = ehrId;
          bla += "\n" + ehrId;
          $("#master").html("EHR: " + ehrId);
          
          var partyData = {
              "firstNames": "Lojze",
              "lastNames": "Miker",
              "gender": "MALE",
              "dateOfBirth": "1990-03-09",
              "address": {
                "address": "Jesenica, Slovenija"
              },
              "additionalInfo": {
                "ehrId" : ehrId,
                "height": "190",
                "weight": "70"
              }
          };
          $.ajax({
              url: baseUrl + "/demographics/party",
              type: 'POST',
              headers: {
                "Authorization": getAuthorization()
              },
              contentType: 'application/json',
              data: JSON.stringify(partyData),
              success: function (party) {
                  if (party.action == 'CREATE') {
                      $("#lead").html("Created: " + party.meta.href);
                      $("#bla").html(bla);
                  }
              },
              error: function(err){
                $("#lead").html("Error: " + JSON.parse(err.responseText).userMessage);
              }
          });
      }
  });*/
  
  
  IzracunajPovprecje();
  graf = [
    {
      x: ['Micka Lolek', 'Karmen Karavel', 'Lojze Miker'],
      y: [26.7, 24.5, 19.4],
      type: 'bar'
    }
  ];
  
  plotaj();
  
  $('#izberiEHR').change(function() {
		$("#Gumb").html("");
		$("#EHR_Id").val($(this).val());
	});
  $("#Generiraj").click(function() {
    for(var i = 1; i < 4; i++){
      generirajPodatke(i);
    }
  });
		
		$.ajax({
        type: "GET",
        url: "https://sl.wikipedia.org/w/api.php?action=parse&format=json&prop=text&section=0&page=Indeks_telesne_mase&callback=?",
        contentType: "application/json; charset=utf-8",
        async: false,
        dataType: "json",
        success: function (data, textStatus, jqXHR) {
 
            var markup = data.parse.text["*"];
            var blurb = $('<div></div>').html(markup);
 
            // remove links as they will not work
            blurb.find('a').each(function() { $(this).replaceWith($(this).html()); });
 
            // remove any references
            blurb.find('sup').remove();
 
            // remove cite error
            blurb.find('.mw-ext-cite-error').remove();
            $('#apiKlic').html($(blurb).find('p'));
        },
        error: function (errorMessage) {
        }
    });
		
  //document.getElementById("izberiEHR").addEventListener("onclick" ,izberiEHR());
});

/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}

/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  ehrId = EHR_ID[stPacienta];
  if(stPacienta == 1){
    $.ajaxSetup({
      headers: {
          "Authorization": getAuthorization()
    }
  });
  $.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {
        "Authorization": getAuthorization()
      },
    	success: function (data) {
  			var party = data.party;
  			var teza = party.additionalInfo.weight;
  			var visina = party.additionalInfo.height;
  			$("#pacient").html("Vzorčni bolnik " + party.firstNames + " " + party.lastNames + " (EHRId: " + ehrId + ") s težo " + 
  			teza + " in višino " + visina + " ima BMI enak: <strong>" + IzracunBMI(teza, visina) +"</strong>");
  			var arr = [visina, teza];
  			visTez.push(arr);
  			IzracunajPovprecje();
  		},
  		error: function(err) {
  			$("#pacient").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
  		}
		});
  
  } else if(stPacienta == 2){
    $.ajaxSetup({
      headers: {
          "Authorization": getAuthorization()
      }
  });
  
  $.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {
        "Authorization": getAuthorization()
      },
    	success: function (data) {
  			var party = data.party;
  			var teza = party.additionalInfo.weight;
  			var visina = party.additionalInfo.height;
  			$("#pacient2").html("Vzorčni bolnik " + party.firstNames + " " + party.lastNames + " (EHRId: " + ehrId + ") s težo " + 
  			teza + " in višino " + visina + " ima BMI enak: <strong>" + IzracunBMI(teza, visina) +"</strong>");
  			var arr = [visina, teza];
  			visTez.push(arr);
  			IzracunajPovprecje();
  		},
  		error: function(err) {
  			$("#pacient2").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
  		}
		});
  
  } else if(stPacienta == 3) {
    $.ajaxSetup({
      headers: {
          "Authorization": getAuthorization()
      }
  });
  
  $.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {
        "Authorization": getAuthorization()
      },
    	success: function (data) {
  			var party = data.party;
  			var teza = party.additionalInfo.weight;
  			var visina = party.additionalInfo.height;
  			$("#pacient3").html("Vzorčni bolnik " + party.firstNames + " " + party.lastNames + " (EHRId: " + ehrId + ") s težo " + 
  			teza + " in višino " + visina + " ima BMI enak: <strong>" + IzracunBMI(teza, visina) +"</strong>");
  		  var arr = [visina, teza];
  			visTez.push(arr);
  			IzracunajPovprecje();
  		},
  		error: function(err) {
  			$("#pacient3").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
  		}
		});
  }

  // TODO: Potrebno implementirati

  return ehrId;
}

function IzracunBMI (teza, visina) {
  visina /= 100;
  visina = visina * visina;
  var del = teza / visina;
  del = Math.round(del * 10) / 10;
  BMI.push(del);
  return del;
}

// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija

function UstvariEHR() {
  var ime = $("#Ime").val();
  var priimek = $("#Priimek").val();
  var teza = $("#Teza").val();
  var visina = $("#Visina").val();
  
  var arr = [visina, teza];
  
  var trigger = preveriGraf(ime + " " + priimek);
  if(trigger == -1) { graf[0].x.push(ime + " " + priimek); graf[0].y.push(IzracunBMI(teza, visina)); visTez.push(arr); stevec++; }
  else              { graf[0].y[trigger] = IzracunBMI(teza, visina); /*visTez[trigger][0] = visina; visTez[trigger][1] = teza;*/ visTez[trigger] = arr; console.log(trigger);}
  
  plotaj();
  
  $.ajaxSetup({
      headers: {
          "Authorization": getAuthorization()
      }
  });
  $.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
          ehrId = data.ehrId;
          IzracunBMI(teza, visina);
          IzracunajPovprecje();
          var partyData = {
              "firstNames": ime,
              "lastNames": priimek,
              address: {
                "address": "Ljubljana, Slovenija"
              },
              additionalInfo: {
                "ehrId" : ehrId,
                "height": teza,
                "weight": visina
              }
          };
          $.ajax({
              url: baseUrl + "/demographics/party",
              type: 'POST',
              headers: {
                "Authorization": getAuthorization()
              },
              contentType: 'application/json',
              data: JSON.stringify(partyData),
              success: function (party) {
                  if (party.action == 'CREATE') {
                    $("#Primer").html("Bolnik " + ime + " " + priimek + " s težo " + 
  		            	teza + " in višino " + visina + " ima BMI enak: <strong>" + IzracunBMI(teza, visina) +"</strong>");
  			            IzpisiNavodila(IzracunBMI(teza, visina));
                    $("#noviId").html("Vaš novi EHR-Id je: " + ehrId);
                  }
              },
              error: function(err){
                $("#noviId").html("Error: " + JSON.parse(err.responseText).userMessage);
              }
          });
      }
  });
}



function obstojecEHR(){
  ehrId = document.getElementById("izberiEHR").value;
  
  $.ajaxSetup({
      headers: {
          "Authorization": getAuthorization()
      }
  });
  $.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {
        "Authorization": getAuthorization()
      },
    	success: function (data) {
  			var party = data.party;
  			var teza = party.additionalInfo.weight;
  			var visina = party.additionalInfo.height;
  			$("#Primer").html("Bolnik " + party.firstNames + " " + party.lastNames + " s težo " + 
  			teza + " in višino " + visina + " ima BMI enak: <strong>" + IzracunBMI(teza, visina) +"</strong>");
  			IzpisiNavodila(IzracunBMI(teza, visina));
  			
  			var arr = [visina, teza];
  			
        var trigger = preveriGraf(party.firstNames + " " + party.lastNames);
        if(trigger == -1) { graf[0].x.push(party.firstNames + " " + party.lastNames); graf[0].y.push(IzracunBMI(teza, visina)); visTez.push(arr);}
        else              { graf[0].y[trigger] = IzracunBMI(teza, visina); visTez[trigger] = arr; }
        
        plotaj();
        
  			IzracunajPovprecje();
  		},
  		error: function(err) {
  			$("#Primer").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
  		}
		});
}

function IzpisiNavodila(bmi){
  var izpisi = "<h5><strong>Prehrana:</strong></h5>";
  var izpisi2 = "<h5><strong>Gibanje:</strong></h5>";

  if(bmi < 21.4){
    izpisi += "Za vašo višino ste podhranjeni, več se prehranjujte, vendar prehranjujte se zdravo!</br>";
    izpisi2 += "Ker se morate več prehranjevati, se morate tudi več gibati, da se vam ne bo nabrala maščoba, vendar mišice!</br>";
  } else if(bmi >= 21.4 && bmi <= 25.6){
    izpisi = "Vaš BMI zgleda v redu, vaša prehrana in gibanje sta dokaj pravilna.</br> To še ne pomeni, da sedaj lahko cele dneve presedite, vsaka odrasla oseba potrebuje vsaj 30 minut intenzivne telovadbe na dan. </br>";
    izpisi2 = "";
  } else if(bmi > 25.6 && bmi <= 30) {
    izpisi += "Za vašo višino ste pretežki, poskušajte se boljše prehranjevati!</br>";
    izpisi2 += "Poskušajte se več gibati!</br>";
  } else {
    izpisi += "Vaš BMI je že v območju debelosti! Prehranjujte se zdravo!</br>";
    izpisi2 += "Veliko več se morate gibati! Sedaj vam bo pomagala že vsak kilogram, ki ga izgubite</br>";
  }
  $("#Izpis1").html(izpisi);
  $("#Izpis2").html(izpisi2);
}

function IzracunajPovprecje(){
  var povp = 0;
  for(var i = 0; i < BMI.length; i++){
    povp += BMI[i];
  }
  povp = povp / BMI.length;
  povp = Math.round(povp * 10) / 10;
  $("#povprecje").html("Trenutno povprecje BMI je: <strong>" + povp + "</strong");
}


/*$.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
          ehrId = data.ehrId;
          EHR_ID[stPacienta] = ehrId;
          bla += "\n" + EHR_ID[stPacienta];
          $("#header").html("EHR: " + ehrId);
          
          var partyData = {
              "firstNames": "Lojze",
              "lastNames": "Miker",
              "gender": "MALE",
              "dateOfBirth": "1990-03-09",
              "address": {
                "address": "Jesenica, Slovenija"
              },
              "additionalInfo": {
                "ehrId" : ehrId,
                "height": "190",
                "weight": "80"
              }
          };
          $.ajax({
              url: baseUrl + "/demographics/party",
              type: 'POST',
              headers: {
                "Authorization": getAuthorization()
              },
              contentType: 'application/json',
              data: JSON.stringify(partyData),
              success: function (party) {
                  if (party.action == 'CREATE') {
                      $("#lead").html("Created: " + party.meta.href);
                      $("#bla").html(bla);
                  }
              },
              error: function(err){
                $("#lead").html("Error: " + JSON.parse(err.responseText).userMessage);
              }
          });
      }
  });*/