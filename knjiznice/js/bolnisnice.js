/* global $ */
/* global L, distance*/

var mapa;
var latlng = L.latLng(0, 0);
var layer;

$(document).ready(function(){
    var mapOptions = {
        center: [46.0680191 , 14.4867966],
        zoom: 15
    };
    mapa = new L.map('mapa_id', mapOptions);
    mapa.on('click', Klik);
    prikaziBolnice();
});

function prikaziBolnice(){
    var barva;
    layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
    mapa.addLayer(layer);
    $.getJSON("./knjiznice/json/bolnisnice.json", function(bolnice) {
        bolnice.features.forEach(function(bolnica){
            if(bolnica.geometry.type == "Polygon"){
                L.geoJSON(bolnica, {
                style: function (feature) {
                    return { color: 
                        barva = distance(bolnica.geometry.coordinates[0][0][1],bolnica.geometry.coordinates[0][0][0], latlng.lat, latlng.lng, "K")  <= 2
                       ? "Green" : "Blue"
                    };
                },
                onEachFeature: function (feature, layer) {
                    layer.bindPopup('<pre>'+JSON.stringify(feature.properties,null,' ').replace(/[\{\}"]/g,'')+'</pre>');
                }
                }).addTo(mapa); 
            }
        });
        latlng =  L.latLng(0, 0);
    });
}

function Klik(e) {
    //var popup = L.popup();
    //var latlng = map.mouseEventToLatLng(ev.originalEvent);
    latlng = e.latlng;
    console.log(latlng.lat + " " + latlng.lng);
    mapa.eachLayer(function(plast){
        if(plast != layer) { mapa.removeLayer(plast); }
    });
    
    prikaziBolnice();
}